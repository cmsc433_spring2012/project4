package server;

import java.io.Serializable;
import java.rmi.RemoteException;

import common.ComputeProducer;
import common.Task;
import common.TaskBag;
import common.TaskKeyPair;

/**
 * An implementation of the TaskBag interface.
 * 
 */
public class TaskBagImpl implements TaskBag {

	/*
	 * (non-Javadoc)
	 * 
	 * @see common.TaskBag#getTask()
	 */
	@Override
	public TaskKeyPair getTask() throws RemoteException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see common.TaskBag#getResultForTask(java.lang.String)
	 */
	@Override
	public Serializable getResultForTask(String taskKey) throws RemoteException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see common.TaskBag#putTask(common.Task, common.ComputeProducer)
	 */
	@Override
	public String putTask(Task task, ComputeProducer submitter)
			throws RemoteException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see common.TaskBag#putResultForTask(java.lang.String,
	 * java.io.Serializable)
	 */
	@Override
	public void putResultForTask(String taskKey, Serializable result)
			throws RemoteException {
	}

	/**
	 * Creates the TaskBag. Creates an RMI registry. Registers the remote
	 * TaskBag reference with the name "TaskBag"
	 * 
	 * @param args
	 *            - TaskBag registry port
	 */
	public static void main(String[] args) {
	}
}

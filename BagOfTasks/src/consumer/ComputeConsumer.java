package consumer;


import common.TaskBag;

/**
 * A ComputeConsumer is a worker class that continuously polls the TaskBag for
 * tasks to execute. For each task it receives, the ComputeConsumer executes the
 * task and then stores the results in the TaskBag.
 */

public class ComputeConsumer {


	/**
	 * @param taskBag
	 *            - remote reference to TaskBag
	 */
	public ComputeConsumer(TaskBag taskBag) {
	}

	/**
	 * Starts and runs one ComputeConsumer.
	 * 
	 * @param args
	 *            - TaskBag Registry host, TaskBag Registry port
	 */
	public static void main(String[] args) {
	}
}

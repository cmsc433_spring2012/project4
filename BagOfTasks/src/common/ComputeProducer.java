package common;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface for clients of the TaskBag that allows the
 * TaskBag to callback to the client when a submitted
 * task has been completed.
 */

public interface ComputeProducer extends Remote {
    /**
     * Callback method called when a previously submitted task completes.
     * 
     * @param taskKey the task's key 
     * @throws RemoteException
     */
    void callBack (String taskKey) throws RemoteException;
}

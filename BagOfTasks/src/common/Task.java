package common;

import java.io.Serializable;

/**
 * Interface for tasks that can be passed to the TaskBag and executed by a
 * ComputeConsumer.
 */

public interface Task extends Serializable {

	/**
	 * Executes the task and returns its result.
	 * 
	 * @return the result.
	 */

	Serializable execute();
}

package common;

import java.io.Serializable;

/**
 * Represents a task and its associated key.
 */

public class TaskKeyPair implements Serializable {

	private static final long serialVersionUID = 1L;
	Task task;
	String key;

	public TaskKeyPair(Task task, String key) {
		this.task = task;
		this.key = key;
	}

	/**
	 * @return the task
	 */
	public Task getTask() {
		return task;
	}

	/**
	 * @return the key for the task
	 */
	public String getKey() {
		return key;
	}
}

package common;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface for interacting with the TaskBag
 */

public interface TaskBag extends Remote {

	/**
	 * Get a task from the TaskBag. Must block called if no tasks are available 
	 * 
	 * @return a task and its key
	 * @throws RemoteException
	 */
	TaskKeyPair getTask() throws RemoteException;

	/**
	 * Returns the result that was computed for the task with key taskKey.
	 * 
	 * @param taskKey
	 *            - the key for a task
	 * @return the result
	 * @throws RemoteException
	 */
	Serializable getResultForTask(String taskKey) throws RemoteException;

	/**
	 * Adds a task to the TaskBag.
	 * 
	 * @param task
	 *            - the task
	 * @param submitter
	 *            - the ComputeProducer submitting the task
	 * @return a unique key for the submitted task
	 * @throws RemoteException
	 */
	String putTask(Task task, ComputeProducer submitter) throws RemoteException;

	/**
	 * Submits the result for the task to the TaskBag.
	 * 
	 * @param taskKey
	 *            - the key for a task
	 * @param result
	 *            - the result
	 * @throws RemoteException
	 */
	void putResultForTask(String taskKey, Serializable result)
			throws RemoteException;

}

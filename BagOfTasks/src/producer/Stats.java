package producer;

import java.rmi.RemoteException;

import common.ComputeProducer;
import common.TaskBag;

/**
 * Generates an array of Integers and uses the TaskBag to compute several
 * statistics over that array.
 * 
 * The statistics tasks to be run are: ACount, BasePercentage, AvgCGDistance
 *
 * The results of all these computations should be printed out before the 
 * Stats class exits.
 */

public class Stats implements ComputeProducer {

	/**
	 * @param taskBag
	 *            - remote reference to the TaskBag
	 */
	public Stats(TaskBag taskBag) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see common.ComputeProducer#callBack(java.lang.String)
	 */
	@Override
	public void callBack(String taskKey) throws RemoteException {
	}


	/**
	 * @param args
	 *            - String filename, Taskbag Registry host, TaskBag Registry port
	 */

	public static void main(String args[]) {
	}
}

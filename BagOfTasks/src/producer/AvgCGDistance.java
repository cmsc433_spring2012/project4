package producer;

import java.io.Serializable;

import common.Task;

/**
 * Finds average distance between consecutive CG positions on the given string.
 */

public class AvgCGDistance implements Task {

	private static final long serialVersionUID = 1L;

	public AvgCGDistance(String data) {
	}

	/* (non-Javadoc)
	 * @see common.Task#execute()
	 * 
	 * The result should be returned as a string of the form:
	 * "AvgCGDistance:" + averageCDdistance
	 */
	public Serializable execute() {
		return null;
	}
}
package producer;

import java.io.Serializable;

import common.Task;

/**
 * Finds the percentage of bases in the string.
 */

public class BasePercentage implements Task {

	private static final long serialVersionUID = 1L;

	public BasePercentage(String data) {
	}

	/* (non-Javadoc)
	 * @see common.Task#execute()
	 * 
	 * The result should be returned as a string of the form:
	 * "BasePercentage:" + percentage of A + percentage of T + percentage of G + percentage of C
	 */
	public Serializable execute() {
		return null;
	}
}
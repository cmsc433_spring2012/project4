package producer;

import java.rmi.RemoteException;

import common.ComputeProducer;
import common.TaskBag;

/**
 * A ComputeProducer that reads in a list of filenames from a given filename, and submits a MotifCountTask
 * to the TaskBag for filename.
 * 
 * Once the MotifCountTasks have finished, this class will merge the results of
 * each task to produce an overall result across all the given filenames.
 * 
 * See MotifCountTask.java for more information about the motif counting process.
 */
public class MotifCountMapReduce implements ComputeProducer {

	/**
	 * @param fileName
	 *            - a file with filenames in it, one per line, each of them motifs counted.
         * @param motif
	 *            - motif that will be counted
	 * @param taskBag
	 *            - remote reference to the TaskBag
	 */
        public MotifCountMapReduce(String fileName, String motif, TaskBag taskBag) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see common.ComputeProducer#callBack(java.lang.String)
	 */
	@Override
	public void callBack(String taskKey) throws RemoteException {
	}

	/**
	 * @param args
	 *            - filename that stores the names of other files, motif sequence, TaskBag Registry host, TaskBag Registry port
	 */
	public static void main(String args[]) {
	}
}

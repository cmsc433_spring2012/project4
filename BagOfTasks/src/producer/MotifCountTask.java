package producer;

import java.io.Serializable;
import java.net.URL;

import common.Task;

/**
 * Reads the given file and counts number of times a given motif is seen on the file.
 */


public class MotifCountTask implements Task {

	private static final long serialVersionUID = 1L;


        /**
	 * @param fileName
	 *            - filename on which a given motif will be counted.
	 * @param motif
	 *            - motif which to be counted on the given file
	 */
        MotifCountTask(String fileName,String motif) {
	}

	/**
	 * Motifs don't need to be exact string. It can also include wildcards. 
	 * 
	 * @see common.Task#execute()
	 */
	@Override
	public Serializable execute() {
		return null;
	}
}
